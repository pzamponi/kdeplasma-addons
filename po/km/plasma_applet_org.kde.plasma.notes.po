# translation of plasma_applet_notes.po to Khmer
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2008, 2009.
# Eng Vannak <evannak@khmeros.info>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-30 00:46+0000\n"
"PO-Revision-Date: 2009-05-29 08:05+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr ""

#: package/contents/ui/configAppearance.qml:36
#, fuzzy, kde-format
#| msgid "Use custom font size:"
msgid "Text font size:"
msgstr "ប្រើ​ទំហំ​ពុម្ពអក្សរ​ផ្ទាល់ខ្លួន ៖"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr ""

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr ""

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr ""

#: package/contents/ui/main.qml:246
#, kde-format
msgid "Undo"
msgstr ""

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Redo"
msgstr ""

#: package/contents/ui/main.qml:264
#, kde-format
msgid "Cut"
msgstr ""

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Copy"
msgstr ""

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Paste Without Formatting"
msgstr ""

#: package/contents/ui/main.qml:286
#, kde-format
msgid "Paste"
msgstr ""

#: package/contents/ui/main.qml:295
#, kde-format
msgid "Delete"
msgstr ""

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Clear"
msgstr ""

#: package/contents/ui/main.qml:312
#, kde-format
msgid "Select All"
msgstr ""

#: package/contents/ui/main.qml:440
#, fuzzy, kde-format
#| msgid "Bold"
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "ដិត"

#: package/contents/ui/main.qml:452
#, fuzzy, kde-format
#| msgid "Italic"
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "ទ្រេត"

#: package/contents/ui/main.qml:464
#, fuzzy, kde-format
#| msgid "Underline"
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "គូស​បន្ទាត់​ពី​ក្រោម"

#: package/contents/ui/main.qml:476
#, fuzzy, kde-format
#| msgid "StrikeOut"
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "ឆូត​"

#: package/contents/ui/main.qml:550
#, kde-format
msgid "Discard this note?"
msgstr ""

#: package/contents/ui/main.qml:551
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr ""

#: package/contents/ui/main.qml:564
#, fuzzy, kde-format
#| msgid "White"
msgctxt "@item:inmenu"
msgid "White"
msgstr "ពណ៌ស"

#: package/contents/ui/main.qml:565
#, fuzzy, kde-format
#| msgid "Black"
msgctxt "@item:inmenu"
msgid "Black"
msgstr "​ខ្មៅ"

#: package/contents/ui/main.qml:566
#, fuzzy, kde-format
#| msgid "Red"
msgctxt "@item:inmenu"
msgid "Red"
msgstr "ក្រហម"

#: package/contents/ui/main.qml:567
#, fuzzy, kde-format
#| msgid "Orange"
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "ទឹកក្រូច"

#: package/contents/ui/main.qml:568
#, fuzzy, kde-format
#| msgid "Yellow"
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "​លឿង"

#: package/contents/ui/main.qml:569
#, fuzzy, kde-format
#| msgid "Green"
msgctxt "@item:inmenu"
msgid "Green"
msgstr "បៃតង"

#: package/contents/ui/main.qml:570
#, fuzzy, kde-format
#| msgid "Blue"
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "ខៀវ"

#: package/contents/ui/main.qml:571
#, fuzzy, kde-format
#| msgid "Pink"
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "ពណ៌ផ្កាឈូក"

#: package/contents/ui/main.qml:572
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "ល្អក់"

#: package/contents/ui/main.qml:573
#, fuzzy, kde-format
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "ល្អក់"

#, fuzzy
#~| msgid "Justify center"
#~ msgid "Align center"
#~ msgstr "តម្រង​សងខាង កណ្ដាល"

#, fuzzy
#~| msgid "Justify"
#~ msgid "Justified"
#~ msgstr "តម្រឹមសងខាង"

#~ msgid "Font"
#~ msgstr "ពុម្ពអក្សរ"

#~ msgid "Style:"
#~ msgstr "រចនាប័ទ្ម ៖"

#~ msgid "&Bold"
#~ msgstr "ដិត"

#~ msgid "&Italic"
#~ msgstr "ទ្រេត"

#~ msgid "Size:"
#~ msgstr "ទំហំ ៖"

#~ msgid "Scale font size by:"
#~ msgstr "ធ្វើ​មាត្រដ្ឋាន​ទំហំ​ពុម្ពអក្សរ​ដោយ ៖"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Color:"
#~ msgstr "ពណ៌ ៖"

#~ msgid "Use theme color"
#~ msgstr "ប្រើ​ពណ៌​ស្បែក"

#~ msgid "Use custom color:"
#~ msgstr "ប្រើ​ពណ៌​ផ្ទាល់ខ្លួន ៖"

#~ msgid "Active line highlight color:"
#~ msgstr "ពណ៌​បន្លិច​បន្ទាត់​សកម្ម ៖"

#~ msgid "Use no color"
#~ msgstr "មិន​ប្រើពណ៌"

#~ msgid "Theme"
#~ msgstr "ស្បែក"

#~ msgid "Notes color:"
#~ msgstr "ពណ៌​ចំណាំ ៖"

#~ msgid "Spell Check"
#~ msgstr "ពិនិត្យអក្ខរាវិរុទ្ធ"

#~ msgid "Enable spell check:"
#~ msgstr "បើក​ការ​ពិនិត្យ​អក្ខរាវិរុទ្ធ ៖"

#~ msgid "Notes Color"
#~ msgstr "ពណ៌​ចំណាំ"

#~ msgid "General"
#~ msgstr "ទូទៅ"

#~ msgid "Formatting"
#~ msgstr "ធ្វើ​​ទ្រង់ទ្រាយ"

#~ msgid "Unable to open file"
#~ msgstr "មិនអាច​បើក​ឯកសារ​បាន​ទេ"

#~ msgid "Welcome to the Notes Plasmoid! Type your notes here..."
#~ msgstr "សូម​ស្វាគមន៍​មក​កាន់​​ចំណាំ​របស់ Plasmoid ! វាយ​ចំណាំ​របស់​អ្នក​នៅ​ទីនេះ..."
