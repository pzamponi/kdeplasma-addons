# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2015, 2016, 2018, 2019.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-10 00:46+0000\n"
"PO-Revision-Date: 2021-12-22 18:05+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.0\n"

#: package/contents/config/config.qml:11
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "Obecné"

#: package/contents/ui/configGeneral.qml:27
#, kde-format
msgctxt "@title:label"
msgid "Username style:"
msgstr "Styl uživatelského jména:"

#: package/contents/ui/configGeneral.qml:30
#, kde-format
msgctxt "@option:radio"
msgid "Full name (if available)"
msgstr "Celé jméno (pokud je dostupné)"

#: package/contents/ui/configGeneral.qml:37
#, kde-format
msgctxt "@option:radio"
msgid "Login username"
msgstr "Přihlašovací jméno"

#: package/contents/ui/configGeneral.qml:55
#, kde-format
msgctxt "@title:label"
msgid "Show:"
msgstr "Zobrazit:"

# Tohle vypadá na název vlastnosti. Jméno bycvh nechal pro lidi a zvířata.
#: package/contents/ui/configGeneral.qml:58
#, kde-format
msgctxt "@option:radio"
msgid "Name"
msgstr "Jméno"

#: package/contents/ui/configGeneral.qml:72
#, kde-format
msgctxt "@option:radio"
msgid "User picture"
msgstr "Obrázek uživatele"

#: package/contents/ui/configGeneral.qml:86
#, kde-format
msgctxt "@option:radio"
msgid "Name and user picture"
msgstr "Jméno a obrázek uživatele"

#: package/contents/ui/configGeneral.qml:105
#, kde-format
msgctxt "@title:label"
msgid "Advanced:"
msgstr "Pokročilé:"

#: package/contents/ui/configGeneral.qml:107
#, kde-format
msgctxt "@option:check"
msgid "Show technical session information"
msgstr "Zobrazit technické informace o sezení"

#: package/contents/ui/main.qml:39
#, kde-format
msgid "You are logged in as <b>%1</b>"
msgstr "Nyní jste přihlášeni jako <b>%1</b>"

#: package/contents/ui/main.qml:134
#, kde-format
msgid "Current user"
msgstr "Aktuální uživatel"

#: package/contents/ui/main.qml:165
#, kde-format
msgctxt "Nobody logged in on that session"
msgid "Unused"
msgstr "Nepoužito"

#: package/contents/ui/main.qml:181
#, kde-format
msgctxt "User logged in on console number"
msgid "TTY %1"
msgstr "TTY %1"

#: package/contents/ui/main.qml:183
#, kde-format
msgctxt "User logged in on console (X display number)"
msgid "on %1 (%2)"
msgstr "na %1 (%2)"

#: package/contents/ui/main.qml:191
#, kde-format
msgctxt "@action:button"
msgid "Switch to User %1"
msgstr ""

#: package/contents/ui/main.qml:200
#, kde-format
msgctxt "@action"
msgid "New Session"
msgstr "Nové sezení"

#: package/contents/ui/main.qml:212
#, kde-format
msgctxt "@action"
msgid "Lock Screen"
msgstr "Uzamknout obrazovku"

#: package/contents/ui/main.qml:224
#, kde-format
msgctxt "Show a dialog with options to logout/shutdown/restart"
msgid "Log Out"
msgstr ""
