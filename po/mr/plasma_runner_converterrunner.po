# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-09 00:47+0000\n"
"PO-Revision-Date: 2013-03-20 17:33+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: Marathi <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"

#: converterrunner.cpp:33
#, kde-format
msgid ""
"Converts the value of :q: when :q: is made up of \"value unit [>, to, as, "
"in] unit\". You can use the Unit converter applet to find all available "
"units."
msgstr ""

#: converterrunner.cpp:42
#, kde-format
msgctxt "list of words that can used as amount of 'unit1' [in|to|as] 'unit2'"
msgid "in;to;as"
msgstr "आत;पर्यंत;असे"

#: converterrunner.cpp:50
#, kde-format
msgid "Copy unit and number"
msgstr ""
