# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# Tommi Nieminen <translator@legisign.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-30 00:46+0000\n"
"PO-Revision-Date: 2022-06-13 19:37+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: calendarsystem.h:53
#, kde-format
msgctxt "@item:inlist"
msgid "Julian"
msgstr "Juliaaninen"

#: calendarsystem.h:57
#, kde-format
msgctxt "@item:inlist"
msgid "Milankovic"
msgstr "Milankovic"

#: calendarsystem.h:61
#, kde-format
msgctxt "@item:inlist"
msgid "The Solar Hijri Calendar (Persian)"
msgstr "Persialainen Hijri-aurinkokalenteri"

#: calendarsystem.h:65
#, kde-format
msgctxt "@item:inlist"
msgid "The Islamic Civil Calendar"
msgstr "Islamilainen maallinen kalenteri"

#: calendarsystem.h:69
#, kde-format
msgctxt "@item:inlist"
msgid "Chinese Lunar Calendar"
msgstr "Kiinalainen kuukalenteri"

#: calendarsystem.h:73
#, kde-format
msgctxt "@item:inlist"
msgid "Indian National Calendar"
msgstr "Intian kansallinen kalenteri"

#: config/qml/AlternateCalendarConfig.qml:38
#, kde-format
msgctxt "@label:listbox"
msgid "Calendar system:"
msgstr "Kalenterijärjestelmä:"

#: config/qml/AlternateCalendarConfig.qml:50
#, kde-format
msgctxt "@label:spinbox"
msgid "Date offset:"
msgstr "Päiväyssiirtymä:"

#: config/qml/AlternateCalendarConfig.qml:59
#, kde-format
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "Yksi päivä"
msgstr[1] "%1 päivää"
