# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-30 00:46+0000\n"
"PO-Revision-Date: 2022-06-04 10:18+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.03.70\n"

#: calendarsystem.h:53
#, kde-format
msgctxt "@item:inlist"
msgid "Julian"
msgstr "Juliański"

#: calendarsystem.h:57
#, kde-format
msgctxt "@item:inlist"
msgid "Milankovic"
msgstr "Milankovic"

#: calendarsystem.h:61
#, kde-format
msgctxt "@item:inlist"
msgid "The Solar Hijri Calendar (Persian)"
msgstr "Słoneczny kalendarz Hijri (perski)"

#: calendarsystem.h:65
#, kde-format
msgctxt "@item:inlist"
msgid "The Islamic Civil Calendar"
msgstr "Islamski kalendarz narodowy"

#: calendarsystem.h:69
#, kde-format
msgctxt "@item:inlist"
msgid "Chinese Lunar Calendar"
msgstr "Chiński kalendarz księżycowy"

#: calendarsystem.h:73
#, kde-format
msgctxt "@item:inlist"
msgid "Indian National Calendar"
msgstr "Indyjski kalendarz narodowy"

#: config/qml/AlternateCalendarConfig.qml:38
#, kde-format
msgctxt "@label:listbox"
msgid "Calendar system:"
msgstr "Układ kalendarza:"

#: config/qml/AlternateCalendarConfig.qml:50
#, kde-format
msgctxt "@label:spinbox"
msgid "Date offset:"
msgstr "Przesunięcie daty:"

#: config/qml/AlternateCalendarConfig.qml:59
#, kde-format
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 dzień"
msgstr[1] "%1 dni"
msgstr[2] "%1 dni"
